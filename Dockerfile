FROM php:5.6-apache

# Install some libs.
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN apt-get update \
    && apt-get install -y libzip-dev \
    && apt-get install -y zlib1g-dev \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install zip

# Install composer.
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && ls | grep composer \
    && php composer-setup.php \
    && mv composer.phar /usr/local/bin/composer

RUN pecl install apcu-4.0.11 && docker-php-ext-enable apcu
RUN a2enmod rewrite
RUN service apache2 restart