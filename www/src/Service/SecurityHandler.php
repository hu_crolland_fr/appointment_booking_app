<?php

namespace AppointmentBookingApp\Service;

use AppointmentBookingApp\Container\InstanceContainer;
use AppointmentBookingApp\DatabaseManager\UserManager;
use AppointmentBookingApp\Model\User;
use function AppointmentBookingAppConfig\config;
use Plasticbrain\FlashMessages\FlashMessages;

/**
 * Class SecurityHandler.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class SecurityHandler
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var FlashMessages
     */
    protected $flashMessage;

    /**
     * @var string
     */
    protected $baseHost;

    public function __construct()
    {
        $this->userManager = InstanceContainer::get(UserManager::class);
        $this->flashMessage = InstanceContainer::get(FlashMessages::class);
        $this->baseHost = config()['routes']['host'];
    }

    public function handleLogin()
    {
        $loginFormData = $_POST['loginForm'];

        if (!isset($loginFormData['username']) || isset($loginFormData['username']) && empty($loginFormData['username'])) {
            $this->flashMessage->error('The username is required.', $this->baseHost);
        }

        if (!isset($loginFormData['password']) || isset($loginFormData['password']) && empty($loginFormData['password'])) {
            $this->flashMessage->error('The password is required.', $this->baseHost);
        }

        $user = $this->userManager->getUserByUsernameAndPassword($loginFormData['username'], $loginFormData['password']);
        if ($user instanceof User) {
            $_SESSION['logged_user'] = $user;

            $this->flashMessage->success(
                'The user was connected successfully.',
                sprintf('%s/appointment', $this->baseHost)
            );
        }

        $this->flashMessage->error('Failed authentication. Incorrect username or password.', $this->baseHost);
    }
}
