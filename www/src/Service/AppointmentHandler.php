<?php

namespace AppointmentBookingApp\Service;

use AppointmentBookingApp\Container\InstanceContainer;
use AppointmentBookingApp\DatabaseManager\AppointmentManager;
use AppointmentBookingApp\DatabaseManager\UserManager;
use AppointmentBookingApp\Model\Appointment;
use AppointmentBookingApp\Router\Router;
use function AppointmentBookingAppConfig\config;
use Plasticbrain\FlashMessages\FlashMessages;

/**
 * Class AppointmentHandler.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class AppointmentHandler
{
    /**
     * @var FlashMessages
     */
    protected $flashMessage;

    /**
     * @var \PDO
     */
    protected $databaseConnection;

    /**
     * @var AppointmentManager
     */
    protected $appointmentManager;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var DateTimeAvailabilities
     */
    protected $dateTimeAvailabilities;

    /**
     * AppointmentHandler constructor.
     *
     * @param $flashMessage
     */
    public function __construct($flashMessage)
    {
        $this->flashMessage = $flashMessage;
        $this->appointmentManager = InstanceContainer::get(AppointmentManager::class);
        $this->userManager = InstanceContainer::get(UserManager::class);
        $this->baseUrl = config()['routes']['host'];
        $this->dateTimeAvailabilities = new DateTimeAvailabilities();
    }

    /**
     * @param Appointment $appointment
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function handleAppointment($appointment)
    {
        $this->checkFieldsValue($appointment);
        $this->checkAppointmentTimeIntervals($appointment);
        $this->checkAppointmentDateTimeAvailabilities($appointment);

        return $this->appointmentManager->insert($appointment);
    }

    /**
     * @param Appointment $appointment
     *
     * @return void
     */
    protected function checkFieldsValue($appointment)
    {
        if (empty($appointment->getTitle())) {
            $this->flashMessage->error('The "Title" field must not be empty.');
        }

        if (empty($appointment->getStartAt())) {
            $this->flashMessage->error('The "Start at" field must not be empty.');
        }

        if (empty($appointment->getEndAt())) {
            $this->flashMessage->error('The "End at" field must not be empty.');
        }

        $this->verifyErrors();
    }

    /**
     * @param Appointment $appointment
     *
     * @return void
     */
    protected function checkAppointmentTimeIntervals($appointment)
    {
        $startTime = new \DateTime($appointment->getStartAt());
        $endTime = new \DateTime($appointment->getEndAt());

        if ($startTime->diff($endTime)->i < 15 && $startTime->diff($endTime)->h === 0) {
            $this->flashMessage->error(sprintf('The appointment time range is short min 15 min is expected, %d min given', $startTime->diff($endTime)->i));
        }

        if ($startTime->diff($endTime)->h > 4) {
            $this->flashMessage->error(sprintf('The appointment time range is large, the time range must not exceed 4 hours, %d hours given.', $startTime->diff($endTime)->h));
        }

        $this->verifyErrors();
    }

    /**
     * @param Appointment $appointmentToCheck
     *
     * @return void
     */
    protected function checkAppointmentDateTimeAvailabilities($appointmentToCheck)
    {
        /* @var Appointment $appointmentItem */
        foreach ($this->appointmentManager->fetchAll() as $appointmentItem) {
            $startAt = (new \DateTime($appointmentToCheck->getStartAt()))->format('Y-m-d H:i:s');
            $endAt =  (new \DateTime($appointmentToCheck->getEndAt()))->format('Y-m-d H:i:s');

            if ($this->dateTimeAvailabilities->dateTimeIsNotAvailable($appointmentItem->getStartAt(), $appointmentItem->getEndAt(), $startAt)) {
                $this->flashMessage->error('The selected start_at time is not available.');
            }

            if ($this->dateTimeAvailabilities->dateTimeIsNotAvailable($appointmentItem->getStartAt(), $appointmentItem->getEndAt(), $endAt)) {
                $this->flashMessage->error('The selected end_at time is not available.');
            }
        }

        $beginAt = $this->dateTimeAvailabilities
            ->dateTimeIsBetweenOpeningHours($appointmentToCheck->getStartAt(), DateTimeAvailabilities::WEEK_DAY)
        ;

        $endAt = $this->dateTimeAvailabilities
            ->dateTimeIsBetweenOpeningHours($appointmentToCheck->getStartAt(), DateTimeAvailabilities::WEEK_END)
        ;

        if (!$beginAt && !$endAt) {
            $this->flashMessage->error('The reservation is allowed between: Monday to Friday (07:00 - 21:00) and Saturday (08:00 - 13:00)');
        }

        $this->verifyErrors();
    }

    /**
     * @return void
     */
    protected function verifyErrors()
    {
        if ($this->flashMessage->hasMessages(FlashMessages::ERROR)) {
            Router::redirectTo(sprintf('%s/appointment', $this->baseUrl));
        }
    }
}
