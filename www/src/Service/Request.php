<?php

namespace AppointmentBookingApp\Service;

/**
 * Class Request.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class Request
{
    const GET = 'GET';
    const POST = 'POST';
    const REQUEST_URI = 'REQUEST_URI';
    const REQUEST_METHOD = 'REQUEST_METHOD';

    /**
     * @var string
     */
    protected $action;

    /**
     * @var string
     */
    protected $requestUri;

    /**
     * @var array
     */
    protected $data = [];

    public function createRequest()
    {
        $this->action = $_SERVER[static::REQUEST_METHOD];
        $this->requestUri = $_SERVER[static::REQUEST_URI];

        switch ($this->action) {
            case static::GET:
                $this->data = $_GET;
                break;
            case static::POST:
                $this->data = $_POST;
                break;
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * This method return the current request URI.
     *
     * @return string
     */
    public function getRequestUri()
    {
        return $this->requestUri;
    }
}
