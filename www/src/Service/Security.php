<?php

namespace AppointmentBookingApp\Service;

use AppointmentBookingApp\Model\User;

/**
 * Class Security.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class Security
{
    /**
     * @return User|null
     */
    public static function getLoggedUser()
    {
        if (isset($_SESSION['logged_user'])) {
            return $_SESSION['logged_user'];
        }

        return null;
    }
}
