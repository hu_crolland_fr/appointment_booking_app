<?php

namespace AppointmentBookingApp\Service;

use AppointmentBookingApp\Model\OpeningHour;

/**
 * Class DateTimeAvailabilities.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class DateTimeAvailabilities
{
    const WEEK_DAY = 'week_day';
    const WEEK_END = 'week_end';

    /**
     * @var OpeningHour[]|array
     */
    protected $openingHours = [];

    /**
     * DateTimeAvailabilities constructor.
     */
    public function __construct()
    {
        $this->openingHours[static::WEEK_DAY] = (new OpeningHour())
            ->setBeginDay('monday')
            ->setEndDay('friday')
            ->setBeginHour('07:00:00')
            ->setEndHour('21:00:00')
        ;

        $this->openingHours[static::WEEK_END] = (new OpeningHour())
            ->setBeginDay('sunday')
            ->setEndDay('sunday')
            ->setBeginHour('08:00:00')
            ->setEndHour('13:00:00')
        ;
    }

    /**
     * This method verify if the current date is between the opening hours.
     *
     * @param string $dateToCheck
     * @param string $period
     *
     * @return bool
     */
    public function dateTimeIsBetweenOpeningHours($dateToCheck, $period)
    {
        $openingHours = isset($this->openingHours[$period]) ? $this->openingHours[$period] : null;
        if (!$openingHours instanceof OpeningHour) {
            return false;
        }

        $startDate = $this->getDay($dateToCheck, $openingHours->getBeginDay());
        $endDate = $this->getDay($dateToCheck, $openingHours->getEndDay());

        $isBetween = ($dateToCheck >= $startDate) && ($dateToCheck <= $endDate);
        if (!$isBetween) {
            return false;
        }

        $timeToCompare = (new \DateTime($dateToCheck))->format('H:i:s');

        return ($timeToCompare >= $openingHours->getBeginHour()) && ($timeToCompare <= $openingHours->getEndHour());
    }

    /**
     * Check ig the datetime is available.
     *
     * @param string $dateBegin
     * @param string $dateEnd
     * @param string $dateToCheck
     *
     * @return bool
     */
    public function dateTimeIsNotAvailable($dateBegin, $dateEnd, $dateToCheck)
    {
        if (($dateToCheck >= $dateBegin) && ($dateToCheck <= $dateEnd)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * This method generate requested day date in the current week.
     *
     * @param string $datetime
     * @param string $day
     *
     * @return string|false
     */
    protected function getDay($datetime, $day)
    {
        return (new \DateTime($datetime))
            ->setTimestamp(strtotime(sprintf('%s this week', $day)))
            ->format('Y-m-d H:i:s')
        ;
    }
}
