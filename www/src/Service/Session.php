<?php

namespace AppointmentBookingApp\Service;

/**
 * Class Session.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class Session
{
    /**
     * @param array $options
     *
     * @return bool
     */
    public static function sessionStart($options = [])
    {
        return session_start($options);
    }

    /**
     * @return void
     */
    public static function sessionDestroy()
    {
        session_destroy();
    }

    /**
     * @return bool
     */
    public static function sessionIsStart()
    {
        return session_status() == PHP_SESSION_NONE;
    }
}
