<?php

namespace AppointmentBookingApp\Router;

use AppointmentBookingApp\Controller\AbstractController;
use AppointmentBookingApp\Service\Request;
use function AppointmentBookingAppConfig\config;

/**
 * Class Router.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class Router
{
    const PROTECT_PAGE = true;
    const REQUEST_URI = 'REQUEST_URI';

    /**
     * @var string
     */
    protected $viewToLoad;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * Router constructor.
     *
     * @param Request $request
     */
    public function __construct($request)
    {
        $this->request = $request;
        $this->baseUrl = config()['routes']['host'];
    }

    /**
     * This method redirect to.
     *
     * @param string $redirectToUrl
     *
     * @return void
     */
    public static function redirectTo($redirectToUrl)
    {
        \header(sprintf('Location: %s', $redirectToUrl), true);

        exit;
    }

    /**
     * This method load the
     *
     * @param string $pattern
     * @param string $controllerName
     * @param string $controllerMethod*
     * @param bool   $protectPage
     *
     * @return void
     */
    public function get($pattern, $controllerName, $controllerMethod, $protectPage = false)
    {
        if ($protectPage && !$_SESSION['logged_user'] instanceof \AppointmentBookingApp\Model\User && $this->request->getRequestUri() !== '/') {
            static::redirectTo($this->baseUrl);
        }

        if ($_SESSION['logged_user'] instanceof \AppointmentBookingApp\Model\User && $this->request->getRequestUri() === '/') {
            static::redirectTo(sprintf('%s/appointment', $this->baseUrl));
        }

        preg_match(sprintf('%s/', $pattern), $this->request->getRequestUri(), $matches);

        if (isset($matches[0]) && sprintf('/%s', $matches[0]) === $this->request->getRequestUri()) {
            /* @var AbstractController $controller */
            $controller = new $controllerName();
            $controller->setRequest($this->request);
            $exploded = explode('/', $this->request->getRequestUri());

            $totalCount = \count($exploded);
            $totalCount > 2 ? $controller->$controllerMethod($exploded[($totalCount - 1)]) : $controller->$controllerMethod();

            $this->viewToLoad = $controller->getCurrentView($controllerMethod);
        }
    }

    /**
     * This method display the requested route view.
     *
     * @return void
     */
    public function boot()
    {
        if (!\is_string($this->viewToLoad)) {
            static::redirectTo(sprintf('%s/page-not-found', $this->viewToLoad));
        }

        echo $this->viewToLoad;
    }
}
