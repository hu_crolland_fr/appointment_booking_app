<?php

namespace AppointmentBookingApp\Router;

/**
 * Class Route.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class Route
{
    /**
     * @var string
     */
    public $patter;

    /**
     * @var bool
     */
    public $protectPage;

    /**
     * @var string
     */
    public $controllerClass;

    /**
     * @var string
     */
    public $controllerMethod;

    /**
     * Route constructor.
     *
     * @param string $patter
     * @param string $controllerClass
     * @param string $controllerMethod
     * @param bool   $protectPage
     */
    public function __construct($patter, $controllerClass, $controllerMethod, $protectPage = false)
    {
        $this->patter = $patter;
        $this->controllerClass = $controllerClass;
        $this->controllerMethod = $controllerMethod;
        $this->protectPage = $protectPage;
    }
}
