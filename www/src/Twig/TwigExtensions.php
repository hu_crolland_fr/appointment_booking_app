<?php

namespace AppointmentBookingApp\Twig;

use AppointmentBookingApp\Container\InstanceContainer;
use AppointmentBookingApp\DatabaseManager\AppointmentManager;
use AppointmentBookingApp\Model\User;
use AppointmentBookingApp\Service\Security;
use Plasticbrain\FlashMessages\FlashMessages;
use Twig\TwigFunction;

/**
 * Class TwigExtensions.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class TwigExtensions
{
    /**
     * @var FlashMessages
     */
    protected $flashMessage;

    /**
     * @var AppointmentManager
     */
    protected $appointmentManager;

    /**
     * TwigExtensions constructor.
     */
    public function __construct()
    {
        $this->flashMessage = InstanceContainer::get(FlashMessages::class);
        $this->appointmentManager = InstanceContainer::get(AppointmentManager::class);
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            'hasFlashMessage' => new TwigFunction('hasFlashMessage', [$this, 'hasFlashMessage']),
            'canManage' => new TwigFunction('canManage', [$this, 'canManage']),
            'loggedUser' => new TwigFunction('loggedUser', [$this, 'loggedUser']),
            'getAppointmentParticipants' => new TwigFunction('getAppointmentParticipants', [$this, 'getAppointmentParticipants']),
            'dump' => new TwigFunction('dump', [$this, 'dump']),
            'flashMessage' => new TwigFunction('flashMessage', [$this, 'flashMessage']),
            'appointmentStatus' => new TwigFunction('appointmentStatus', [$this, 'appointmentStatus']),
        ];
    }

    /**
     * @param string|null $messageType
     *
     * @return void
     */
    public function flashMessage($messageType = null)
    {
        if (\is_string($messageType)) {
            $this->flashMessage->display($messageType);
        } else {
            $this->flashMessage->display();
        }
    }

    /**
     * @param string|null $messageType
     *
     * @return bool
     */
    public function hasFlashMessage($messageType = null)
    {
        return \is_string($messageType) ? $this->flashMessage->hasMessages($messageType) : $this->flashMessage->hasMessages();
    }

    /**
     * @param string $userRole
     *
     * @return bool
     */
    public function canManage($userRole) {
        /* @var User $user */
        return Security::getLoggedUser()->getRoles() === $userRole;
    }

    /**
     * @return User|null
     */
    public function loggedUser()
    {
        return Security::getLoggedUser();
    }

    /**
     * @param int $appointmentId
     *
     * @return array
     */
    public function getAppointmentParticipants($appointmentId)
    {
        return $this->appointmentManager->getParticipantsByAppointmentId($appointmentId);
    }

    /**
     * @param int $status
     */
    public function appointmentStatus($status)
    {
        switch ((int) $status) {
            case 0 :
                return 'To Validate';
            case 1 :
                return 'Approved';
            case 2 :
                return 'Refused';
            default:
                return '';
        }
    }

    /**
     * @param mixed $vars
     *
     * @return void
     */
    public function dump(&$vars)
    {
        dump($vars);
    }
}
