<?php

namespace AppointmentBookingApp\Container;

/**
 * Class InstanceContainer.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class InstanceContainer implements InstanceContainerInterface
{
    /**
     * @var
     */
    protected static $container = [];

    public static function set($key, $object)
    {
        if (\array_key_exists($key, static::$container)) {
            return;
        }

        static::$container[$key] = $object;
    }

    /**
     * This method return the requested instance by instanceId.
     * The id of each instance is the class name.
     *
     * Example : InstanceContainer::get(Object::class)
     *
     * @param string $instanceId
     *
     * @return object|null
     */
    public static function get($instanceId)
    {
        foreach (static::$container as $key => $service) {
            if ($key === $instanceId) {
                return $service;
            }
        }

        return null;
    }

    /**
     * This method verify if the requested instance exist.
     *
     * Example : InstanceContainer::has(Object::class)
     *
     * @param string $instanceId
     *
     * @return bool
     */
    public static function has($instanceId)
    {
        // TODO: Implement has() method.
    }
}
