<?php

namespace AppointmentBookingApp\Container;

/**
 * Interface InstanceContainerInterface.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
interface InstanceContainerInterface
{
    /**
     * This method return the requested instance by instanceId.
     * The id of each instance is the class name.
     *
     * Example : InstanceContainer::get(Object::class)
     *
     * @param string $instanceId
     *
     * @return object|null
     */
    public static function get($instanceId);

    /**
     * This method verify if the requested instance exist.
     *
     * Example : InstanceContainer::has(Object::class)
     *
     * @param string $instanceId
     *
     * @return bool
     */
    public static function has($instanceId);
}
