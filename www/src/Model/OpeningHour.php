<?php

namespace AppointmentBookingApp\Model;

/**
 * Class OpeningHour.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class OpeningHour
{
    /**
     * @var string
     */
    private $beginDay;

    /**
     * @var string
     */
    private $endDay;

    /**
     * @var string
     */
    private $beginHour;

    /**
     * @var string
     */
    private $endHour;

    /**
     * @return string
     */
    public function getBeginDay()
    {
        return $this->beginDay;
    }

    /**
     * @param string $beginDay
     *
     * @return OpeningHour
     */
    public function setBeginDay($beginDay)
    {
        $this->beginDay = $beginDay;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndDay()
    {
        return $this->endDay;
    }

    /**
     * @param string $endDay
     *
     * @return OpeningHour
     */
    public function setEndDay($endDay)
    {
        $this->endDay = $endDay;

        return $this;
    }

    /**
     * @return string
     */
    public function getBeginHour()
    {
        return $this->beginHour;
    }

    /**
     * @param string $beginHour
     * @return OpeningHour
     */
    public function setBeginHour($beginHour)
    {
        $this->beginHour = $beginHour;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndHour()
    {
        return $this->endHour;
    }

    /**
     * @param string $endHour
     *
     * @return OpeningHour
     */
    public function setEndHour($endHour)
    {
        $this->endHour = $endHour;

        return $this;
    }
}
