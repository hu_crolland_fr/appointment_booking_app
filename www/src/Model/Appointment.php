<?php

namespace AppointmentBookingApp\Model;

/**
 * Class Appointment.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class Appointment
{
    /*
     * The different status of an appointment life cycle.
     */
    const IN_PROGRESS_STATUS = 0;
    const APPROVE_STATUS = 1;
    const REFUSE_STATUS = 2;

    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $title;

    /**
     * @var \DateTimeInterface|null
     */
    protected $startAt;

    /**
     * @var \DateTimeInterface|null
     */
    protected $endAt;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    protected $userId;

    /**
     * @var array
     */
    protected $participantsId;

    public function hydrate(array $data = [])
    {
        $this->title = isset($data['title']) ? $data['title'] : null;
        $this->participantsId = isset($data['participants']) ? $data['participants'] : [];
        $this->startAt = isset($data['start_at']) ? (new \DateTime($data['start_at']))->format('Y-m-d H:i:s'): null;
        $this->endAt = isset($data['end_at']) ? (new \DateTime($data['end_at']))->format('Y-m-d H:i:s'): null;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return Appointment
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     *
     * @return Appointment
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param \DateTimeInterface|null $startAt
     *
     * @return Appointment
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @param \DateTimeInterface|null $endAt
     *
     * @return Appointment
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     *
     * @return Appointment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return array
     */
    public function getParticipantsId()
    {
        return $this->participantsId;
    }

    /**
     * @param array $participantsId
     *
     * @return Appointment
     */
    public function setParticipantsId($participantsId)
    {
        $this->participantsId = $participantsId;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     *
     * @return Appointment
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }
}
