<?php

namespace AppointmentBookingApp\Controller;

use AppointmentBookingApp\Container\InstanceContainer;
use AppointmentBookingApp\DatabaseManager\AppointmentManager;
use AppointmentBookingApp\DatabaseManager\UserManager;
use AppointmentBookingApp\Model\Appointment;
use AppointmentBookingApp\Service\AppointmentHandler;

/**
 * Class AppointmentController.
 *
 * @author Rolland Csatari <rolland.csatari@ekino.com>
 */
class AppointmentController extends AbstractController
{
    /**
     * @var AppointmentManager
     */
    protected $appointmentManager;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var AppointmentHandler
     */
    protected $appointmentHandler;

    /**
     * @var string
     */
    private $appointmentViewUrl;

    public function __construct()
    {
        parent::__construct();

        $this->appointmentManager = InstanceContainer::get(AppointmentManager::class);
        $this->userManager = InstanceContainer::get(UserManager::class);
        $this->appointmentHandler = new AppointmentHandler($this->flashMessage);
        $this->appointmentViewUrl = sprintf('%s/appointment', $this->baseHost);
    }

    /**
     * Generate appointment page view.
     *
     * @return void
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function appointmentPage()
    {
        if ($this->getRequest()->getAction() === 'POST' && $_POST['bookingForm']) {
            $appointment = (new Appointment())->hydrate($_POST['bookingForm']);

            $result = $this->appointmentHandler->handleAppointment($appointment);
            $result ? $this->flashMessage->success('The appointment was created successfully.', $this->appointmentViewUrl) : $this->flashMessage->error('The appointment creation was failed', $this->appointmentViewUrl);
        }

        $this->render('appointment.html.twig', [
            'appointments' =>  $this->appointmentManager->fetchAll(),
            'dateNow'      => (new \DateTime())->format('Y-m-d'),
            'formAction'   => $this->appointmentViewUrl,
            'users'        => $this->userManager->fetchAll(),
        ]);
    }

    /**
     * This action remove the requested appointment.
     *
     * @param string $id
     *
     * @return void
     */
    public function removeAppointmentAction($id)
    {
        if ($this->appointmentManager->deleteAppointment((int) $id)) {
            $this->flashMessage->success('Appointment was removed successfully', $this->appointmentViewUrl);
        }

        $this->flashMessage->error('Appointment removed was failed', $this->appointmentViewUrl);
    }

    /**
     * This action approve the requested appointment.
     *
     * @param string $id
     *
     * @return void
     */
    public function approveAppointmentAction($id)
    {
        if ($this->appointmentManager->approveAppointment((int) $id)) {
            $this->flashMessage->success('Appointment was approved successfully', $this->appointmentViewUrl);
        }

        $this->flashMessage->error('Appointment approved was failed', $this->appointmentViewUrl);
    }

    /**
     * This action refuse the requested appointment.
     *
     * @param string $id
     *
     * @return void
     */
    public function refuseAppointmentAction($id)
    {
        if ($this->appointmentManager->refuseAppointment((int) $id)) {
            $this->flashMessage->success('Appointment was refuse successfully', $this->appointmentViewUrl);
        }

        $this->flashMessage->error('Appointment refuse was failed', $this->appointmentViewUrl);
    }
}
