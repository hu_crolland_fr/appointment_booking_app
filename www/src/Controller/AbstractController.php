<?php

namespace AppointmentBookingApp\Controller;

use AppointmentBookingApp\Container\InstanceContainer;
use AppointmentBookingApp\Service\Request;
use function AppointmentBookingAppConfig\config;
use Plasticbrain\FlashMessages\FlashMessages;
use Twig\Environment;

/**
 * Class AbstractController.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
abstract class AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var string|null
     */
    protected $currentView;

    /**
     * @var FlashMessages
     */
    protected $flashMessage;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var static
     */
    protected $baseHost;

    /**
     * AbstractController constructor.
     */
    public function __construct()
    {
        $this->twig = InstanceContainer::get(Environment::class);
        $this->flashMessage = InstanceContainer::get(FlashMessages::class);
        $this->baseHost = config()['routes']['host'];
    }

    /**
     * @param string $template
     * @param array  $parameters
     *
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function render($template, $parameters = [])
    {
        $this->currentView = $this->twig->render($template, $parameters);
    }

    /**
     * @return string|null
     */
    public function getCurrentView()
    {
        return $this->currentView;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     *
     * @return AbstractController
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

}
