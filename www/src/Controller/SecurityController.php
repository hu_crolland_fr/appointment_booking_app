<?php

namespace AppointmentBookingApp\Controller;

use AppointmentBookingApp\Model\User;
use AppointmentBookingApp\Router\Router;
use AppointmentBookingApp\Service\Security;
use AppointmentBookingApp\Service\SecurityHandler;
use AppointmentBookingApp\Service\Session;

/**
 * Class SecurityController.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class SecurityController extends AbstractController
{
    /**
     * @var SecurityHandler
     */
    protected $securityHandler;

    /**
     * SecurityController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->securityHandler = new SecurityHandler();
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @return void
     */
    public function loginPage()
    {
        if (Security::getLoggedUser() instanceof User) {
            $this->flashMessage->info('The user is already connected.', sprintf('%s/appointment', $this->baseHost));
        }

        if ($this->getRequest()->getAction() === 'POST' && isset($_POST['loginForm'])) {
            $this->securityHandler->handleLogin();
        }

        $this->render('login.html.twig', [
            'formAction' => $this->baseHost,
        ]);
    }

    /**
     * Destroy session and redirect to login page.
     *
     * @return void
     */
    public function logoutPage()
    {
        Session::sessionDestroy();

        Router::redirectTo($this->baseHost);
    }
}
