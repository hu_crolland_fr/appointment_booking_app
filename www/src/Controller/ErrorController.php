<?php

namespace AppointmentBookingApp\Controller;

/**
 * Class ErrorController.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class ErrorController extends AbstractController
{
    /**
     * This action render the not found page.
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @return void
     */
    public function pageNotFoundPage()
    {
        $this->render('errors/page_not_found.html.twig');
    }
}
