<?php

namespace AppointmentBookingApp\DatabaseManager;

use AppointmentBookingApp\Model\User;

/**
 * Class UserManager.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class UserManager
{
    /**
     * @var \PDO
     */
    protected $databaseConnection;

    /**
     * @param DatabaseConnection $databaseConnection
     *
     * @throws \Exception
     *
     * @return self
     */
    public function setDatabaseConnection($databaseConnection)
    {
        if (!$databaseConnection instanceof DatabaseConnection) {
            throw new \Exception('The database connection must be set UserManager->setDatabaseConnection()');
        }

        $this->databaseConnection = $databaseConnection->getDatabaseConnection();

        return $this;
    }

    public function getUserByUsernameAndPassword($username, $password)
    {
        $statement = $this->databaseConnection
            ->prepare('SELECT id, fullname, username, roles FROM users WHERE username = :username AND password = :password')
        ;

        $statement->execute([
            'username' => $username,
            'password' => $password,
        ]);

        return $statement->fetchObject(User::class);
    }

    public function fetchAll()
    {
        $statement = $this->databaseConnection
            ->prepare('SELECT id, fullname FROM users')
        ;

        $statement->execute();
        $statement->setFetchMode(\PDO::FETCH_CLASS,User::class);

        return $statement->fetchAll();
    }
}
