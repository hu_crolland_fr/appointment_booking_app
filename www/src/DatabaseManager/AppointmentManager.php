<?php

namespace AppointmentBookingApp\DatabaseManager;

use AppointmentBookingApp\Model\Appointment;
use AppointmentBookingApp\Model\User;
use AppointmentBookingApp\Service\Security;

/**
 * Class AppointmentManager.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class AppointmentManager
{
    /**
     * @var \PDO
     */
    protected $databaseConnection;

    /**
     * @param DatabaseConnection $databaseConnection
     *
     * @throws \Exception
     *
     * @return self
     */
    public function setDatabaseConnection($databaseConnection)
    {
        if (!$databaseConnection instanceof DatabaseConnection) {
            throw new \Exception('The database connection must be set AppointmentManager->setDatabaseConnection()');
        }

        $this->databaseConnection = $databaseConnection->getDatabaseConnection();

        return $this;
    }

    /**
     * @return array
     */
    public function fetchAll()
    {
        $statement = $this->databaseConnection
            ->prepare('SELECT id, title, start_at as startAt, end_at as endAt, status, user_id as userId  FROM appointment ')
        ;

        $statement->execute();

        $statement->setFetchMode(\PDO::FETCH_CLASS,Appointment::class);

        return $statement->fetchAll();
    }

    public function fetchAppointmentByDateTime($dateTime)
    {
        $statement = $this->databaseConnection
            ->prepare('WHERE start_at >= :begin_at AND start_at <= :end_at_date')
        ;

        $statement->execute([
            'begin_at' => sprintf('%s 07:00', (new \DateTime($dateTime))->format('Y-m-d')),
            'end_at'   => sprintf('%s 21:00', (new \DateTime($dateTime))->format('Y-m-d')),
        ]);

        $statement->setFetchMode(\PDO::FETCH_CLASS,Appointment::class);

        return $statement->fetchAll();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function findOneById($id)
    {
        $statement = $this->databaseConnection
            ->prepare('SELECT id, title, start_at as startAt, end_at as endAt, status FROM appointment WHERE id = :id')
        ;

        $statement->execute([
            'id' => $id,
        ]);

        return $statement->fetchObject(Appointment::class);
    }

    /**
     * @param string $query
     * @param array  $params
     *
     * @return bool
     */
    private function execute($query, $params)
    {
        $statement = $this->databaseConnection->prepare($query);

        try {
            $this->databaseConnection->beginTransaction();
            $statement->execute($params);
            $this->databaseConnection->commit();

            return true;

        } catch (\Exception $exception) {
            $this->databaseConnection->rollBack();

            return false;
        }
    }

    /**
     * @param Appointment $appointment
     *
     * @return array
     *
     * @throws \Exception
     */
    public function insert($appointment)
    {
        $response = [
            'appointmentInsertion'             => false,
            'appointmentParticipantsInsertion' => false,
        ];

        $appointmentInsertion = $this->execute('INSERT into appointment (title, start_at, end_at, user_id) VALUES (:title, :start_at, :end_at, :user_id)', [
            'title'    => $appointment->getTitle(),
            'start_at' => $appointment->getStartAt(),
            'end_at'   => $appointment->getEndAt(),
            'user_id'  => Security::getLoggedUser()->getId(),
        ]);

        if (!$appointmentInsertion) {
            return $response;
        }

        $response['appointmentInsertion'] = true;

        $values = '';
        $result = $this->databaseConnection->query('SELECT MAX(id) as lastInsertId from appointment LIMIT 1')->fetch();
        foreach ($appointment->getParticipantsId() as $key => $userId) {
            $comma = \count($appointment->getParticipantsId()) !== ($key + 1) ? ',' : '';
            $values = sprintf('%s (%d, %s)%s', $values, $userId, $result['lastInsertId'], $comma);
        }

        $appointmentParticipantsInsertion = $this->execute(sprintf('INSERT into appointment_participants (user_id, appointment_id) VALUES %s', $values), []);

        $response['appointmentParticipantsInsertion'] = $appointmentParticipantsInsertion;

        return $response;
    }

    /**
     * This query remove the appointment from database.
     *
     * @param int $appointmentId
     *
     * @return bool
     */
    public function deleteAppointment($appointmentId)
    {
        return $this->execute('DELETE FROM appointment WHERE id = :id', [
            'id' => $appointmentId,
        ]);
    }

    /**
     * This query update the appointment status to "Refused"
     *
     * @param int $appointment
     *
     * @return bool
     */
    public function refuseAppointment($appointmentId)
    {
        return $this->execute('UPDATE appointment SET status = :status WHERE id = :id', [
            'id'     => $appointmentId,
            'status' => Appointment::REFUSE_STATUS,
        ]);
    }

    /**
     * This query update the appointment status to "Approved"
     *
     * @param int $appointmentId
     *
     * @return bool
     */
    public function approveAppointment($appointmentId)
    {
        return $this->execute('UPDATE appointment SET status = :status WHERE id = :id', [
            'id'     => $appointmentId,
            'status' => Appointment::APPROVE_STATUS,
        ]);
    }

    public function getParticipantsByAppointmentId($appointmentId)
    {
        $statement = $this->databaseConnection
            ->prepare('SELECT fullname from users INNER JOIN appointment_participants a on users.id = a.user_id WHERE a.appointment_id = :appointment_id')
        ;

        $statement->execute([
            'appointment_id' => (int) $appointmentId,
        ]);

        $statement->setFetchMode(\PDO::FETCH_CLASS, User::class);

        return $statement->fetchAll();
    }
}
