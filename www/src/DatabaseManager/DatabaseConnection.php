<?php

namespace AppointmentBookingApp\DatabaseManager;

use function AppointmentBookingAppConfig\config;

/**
 * Class DatabaseConnection.
 *
 * @author Rolland Csatari <rolland.csatari@gmail.com>
 */
class DatabaseConnection
{
    /**
     * @var \PDO
     */
    protected $databaseConnection;

    public function __construct()
    {
        $this->verifyDatabaseConfigurations();

        /* Build the DSN. */
        $dsn = sprintf('mysql:host=db;port=3306;dbname=%s;charset=utf8',
            config()['database']['DATABASE']
        );

        $this->databaseConnection = new \PDO($dsn, config()['database']['USER'], config()['database']['PASSWORD']);
        $this->databaseConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @return \PDO
     */
    public function getDatabaseConnection()
    {
        return $this->databaseConnection;
    }

    /**
     * This method verify the database configuration.
     *
     * @throws \Exception
     *
     * @return void
     */
    private function verifyDatabaseConfigurations()
    {
        /* Check if the database configuration was defined. */
        if (!isset(config()['database'])) {

            throw new \Exception('The database configuration was not defined.');
        /* Check if the database configuration contains all required database parameters.  */
        } elseif (!isset(config()['database']['HOST']) || !isset(config()['database']['USER']) || !isset(config()['database']['PASSWORD']) || !isset(config()['database']['ROOT_PASSWORD']) || !isset(config()['database']['DATABASE'])) {

            throw new \Exception(sprintf('The following database configurations must be defined : %s', 'HOST, USER, PASSWORD, ROOT_PASSWORD, DATABASE'));
        }
    }
}
