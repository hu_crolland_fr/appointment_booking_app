<?php

namespace AppointmentBookingAppConfig;

use AppointmentBookingApp\Router\Route;

/**
 * @return array
 */
function config()
{
    $config = [];

    $config['base']['base_dir'] = __DIR__. '/..';

    /* Database configurations. */
    $config['database']['USER'] = 'appointment_booking_app';
    $config['database']['PASSWORD'] = 'appointment_booking_app';
    $config['database']['ROOT_PASSWORD'] = 'appointment_booking_app';
    $config['database']['DATABASE'] = 'appointment_booking_app';
    $config['database']['HOST'] = '127.0.0.1';

    /* Routes configuration */
    $config['routes']['host'] = 'http://localhost:8080';

    $config['routes']['declaration'] = [
        new Route('/', \AppointmentBookingApp\Controller\SecurityController::class, 'loginPage'),
        new Route('/login_check', \AppointmentBookingApp\Controller\SecurityController::class, 'loginCheckPage'),
        new Route('/logout', \AppointmentBookingApp\Controller\SecurityController::class, 'logoutPage'),
        new Route('/appointment', \AppointmentBookingApp\Controller\AppointmentController::class, 'appointmentPage', true),
        new Route('/appointment\/remove\/(\d)+', \AppointmentBookingApp\Controller\AppointmentController::class, 'removeAppointmentAction', true),
        new Route('/appointment\/approve\/(\d)+', \AppointmentBookingApp\Controller\AppointmentController::class, 'approveAppointmentAction', true),
        new Route('/appointment\/refuse\/(\d)+', \AppointmentBookingApp\Controller\AppointmentController::class, 'refuseAppointmentAction', true),
        new Route('/page-not-found', \AppointmentBookingApp\Controller\ErrorController::class, 'pageNotFoundPage'),
    ];

    $config['twig']['template_dir_path']             = sprintf('%s/templates', $config['base']['base_dir']);
    $config['twig']['environment']['cache_dir_path'] = sprintf('%s/cache/twig', $config['base']['base_dir']);;

    return $config;
}
