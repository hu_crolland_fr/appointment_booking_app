<?php

namespace AppointmentBookingAppConfig;

use AppointmentBookingApp\Container\InstanceContainer;
use AppointmentBookingApp\DatabaseManager\AppointmentManager;
use AppointmentBookingApp\DatabaseManager\UserManager;
use AppointmentBookingApp\DatabaseManager\DatabaseConnection;
use AppointmentBookingApp\Router\Router;
use AppointmentBookingApp\Twig\TwigExtensions;
use Plasticbrain\FlashMessages\FlashMessages;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * This function load all twig extension declared in \AppointmentBookingApp\Twig\TwigExtensions.
 *
 * @return Environment
 */
function loadTwigFunctions() {
    $twigEnvironment = new Environment(new FilesystemLoader(config()['twig']['template_dir_path']), config()['twig']['environment']);

    $twigFunctions = new TwigExtensions();
    foreach ($twigFunctions->getFunctions() as $key => $function) {
        $twigEnvironment->addFunction($key, $function);
    }

    return $twigEnvironment;
}

/**
 * Init the global service container.
 *
 * @throws \Exception
 *
 * @return void
 */
function loadInstances() {
    global $serviceContainer;
    $serviceContainer = new InstanceContainer();

    $serviceContainer::set(DatabaseConnection::class, new DatabaseConnection());
    $serviceContainer::set(FlashMessages::class, new FlashMessages());
    $serviceContainer::set(AppointmentManager::class, (new AppointmentManager())->setDatabaseConnection($serviceContainer::get(DatabaseConnection::class)));
    $serviceContainer::set(UserManager::class, (new UserManager())->setDatabaseConnection($serviceContainer::get(DatabaseConnection::class)));
    $serviceContainer::set(Environment::class, loadTwigFunctions());
}

/**
 * This method load the view of the requested route.
 *
 * @param Router $router
 *
 * @return void
 */
function dispatchRoute($router) {
    foreach (config()['routes']['declaration'] as $route) {
        $router->get($route->patter, $route->controllerClass, $route->controllerMethod, $route->protectPage);
    }
}

