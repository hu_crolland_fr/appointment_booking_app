<?php

date_default_timezone_set('Europe/Budapest');

/* =================================
 *  Load vendor dependencies.
 * ================================= */

require_once './vendor/autoload.php';

/* =================================
 *  Start app session.
 * ================================= */

\AppointmentBookingApp\Service\Session::sessionStart();

/* =================================
 *  Load service container.
 * ================================= */

\AppointmentBookingAppConfig\loadInstances();

/* =================================
 *  Load the requested route.
 * ================================= */

$request = new \AppointmentBookingApp\Service\Request();
$request->createRequest();

$router = new \AppointmentBookingApp\Router\Router($request);
\AppointmentBookingAppConfig\dispatchRoute($router);
$router->boot();


$memoryLogger['after'] = round(memory_get_usage() / 1024);
