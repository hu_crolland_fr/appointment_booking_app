## Appointment Booking APP

**Required**: Docker

##### Install

To install and run the project execute the following commands:
```
make docker-instra-start
make app-install
```

The following command `make docker-instra-start` at the first install
pull the docker image and init the infrastructure, after just start
the docker containers.

The following command `app-install` install the php vendors.

##### Some URLs:

Locale app : `http://localhost:8080` <br />
Locale phpmyadmin: `http://localhost:8181` <br />

##### Access

User with ADMIN_ROLE ```admin:admin ``` <br />
User with USER_ROLE  ```szollos.beatrix:user```  <br />
phpMyAdmin login ```appointment_booking_app:appointment_booking_app```  <br />

