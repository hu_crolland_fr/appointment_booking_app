help:
	@echo ""
	@echo "Appointment Booking App commands"
	@echo ""
	@echo "docker-infra-start	This command start the docker infrastructure."
	@echo "docker-infra-stop	This command stop the docker infrastructure."
	@echo "app-install		This command install app vendors."
	@echo "app-infra-shell         This command connect to shell of the web server"
	@echo ""

docker-infra-start:
	docker-compose up -d

docker-infra-stop:
	docker-compose stop

app-install:
	docker exec appointment_booking_app_www_1 composer install

app-infra-shell:
	docker exec -it appointment_booking_app_www_1 bash
